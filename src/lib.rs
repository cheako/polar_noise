extern crate noise;

use noise::{NoiseFn, Point2, Point3, Point4};

macro_rules! map {
    // Historically I've bad luck getting these things right, so test this!!!
    ( $v:expr, $a1:expr, $a2:expr, $b1:expr, $b2:expr ) => ( ($v - $a1 as f64) * ( ($b2 as f64 - $b1 as f64) / ($a2 as f64 - $a1 as f64) ) + $b1 as f64) 
}

#[derive(Clone, Copy, Debug)]
pub struct PolarNoise1D<T: NoiseFn<Point2<f64>>> {
    // Radius, NOT the diamiter.
    r: f64,
    // Center, but actually it's the top left cornor of the bounding box.
    cx: f64,
    cy: f64,
    noise: T,
}

impl<T> NoiseFn<f64> for PolarNoise1D<T> 
    where T: NoiseFn<Point2<f64>> {
    fn get(&self, point: f64) -> f64 {
        let offset = point.sin_cos();
        self.noise.get([
            map!(offset.0, -1, 1, self.cx, self.cx + self.r * 2.0),
            map!(offset.1, -1, 1, self.cy, self.cy + self.r * 2.0),
        ])
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PolarNoise2D<T: NoiseFn<Point3<f64>>> {
    // Radius, NOT the diamiter.
    r: f64,
    // Center, but actually it's the top left cornor of the bounding box.
    cx: f64,
    cy: f64,
    cz: f64,
    noise: T,
}

impl<T> NoiseFn<Point2<f64>> for PolarNoise2D<T> 
    where T: NoiseFn<Point3<f64>> {
    // As in https://stackoverflow.com/a/30011781/1153319
    fn get(&self, point: Point2<f64>) -> f64 {
        let mut offset = [
            point[0].sin_cos(),
            point[1].sin_cos(),
        ];
        offset[0].0 *= offset[1].1;
        offset[0].1 *= offset[1].1;
        self.noise.get([
            map!(offset[0].0, -1, 1, self.cx, self.cx + self.r * 2.0),
            map!(offset[0].1, -1, 1, self.cy, self.cy + self.r * 2.0),
            map!(offset[1].0, -1, 1, self.cz, self.cz + self.r * 2.0),
        ])
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PolarNoise3D<T: NoiseFn<Point4<f64>>> {
    // Radius, NOT the diamiter.
    r: f64,
    // Center, but actually it's the top left cornor of the bounding box.
    cx: f64,
    cy: f64,
    cz: f64,
    cw: f64,
    noise: T,
}

impl<T> NoiseFn<Point3<f64>> for PolarNoise3D<T> 
    where T: NoiseFn<Point4<f64>> {
    // As in https://stackoverflow.com/questions/30011741/3d-vector-defined-by-2-angles#comment97525937_30011781
    fn get(&self, point: Point3<f64>) -> f64 {
        let mut offset = [
            point[0].sin_cos(),
            point[1].sin_cos(),
            point[2].sin_cos(),
        ];
        offset[1].0 *= offset[2].1;
        offset[1].1 *= offset[2].1;
        offset[0].0 *= offset[1].1;
        offset[0].1 *= offset[1].1;
        self.noise.get([
            map!(offset[0].0, -1, 1, self.cx, self.cx + self.r * 2.0),
            map!(offset[0].1, -1, 1, self.cy, self.cy + self.r * 2.0),
            map!(offset[1].0, -1, 1, self.cz, self.cz + self.r * 2.0),
            map!(offset[2].0, -1, 1, self.cw, self.cw + self.r * 2.0),
        ])
    }
}

#[cfg(test)]
mod tests {
    use noise::{OpenSimplex, NoiseFn};

    #[test]
    fn one_dimension() {
        let t = crate::PolarNoise1D {
                r: 5.0,
                cx: 0.0,
                cy: 0.0,
                noise: OpenSimplex::default(),
        };
        assert_eq!(t.get(0.0), -0.23408686044787888);
        assert_eq!(t.get(1.0),  0.07062771603954277);
        assert_eq!(t.get(2.0),  0.24217036371908332);
    }

    #[test]
    fn two_dimension() {
        let t = crate::PolarNoise2D {
                r: 5.0,
                cx: 0.0,
                cy: 0.0,
                cz: 0.0,
                noise: OpenSimplex::default(),
        };
        assert_eq!(t.get([0.0,0.0]), -0.19331040852222237);
        assert_eq!(t.get([1.0,0.0]), -0.15238871045619296);
        assert_eq!(t.get([2.0,0.0]),  0.17925086965575293);
    }

    #[test]
    fn three_dimension() {
        let t = crate::PolarNoise3D {
                r: 5.0,
                cx: 0.0,
                cy: 0.0,
                cz: 0.0,
                cw: 0.0,
                noise: OpenSimplex::default(),
        };
        assert_eq!(t.get([0.0,0.0,0.0]), -0.06256822347964933);
        assert_eq!(t.get([1.0,0.0,0.0]),  0.2269121944975952);
        assert_eq!(t.get([2.0,0.0,0.0]), -0.7210806157292607);
    }
}